#!/bin/sh
#
# 'moo-cow' is a simple shell script to directly download Jamendo albums
# and unpack them in a way that can be than played and seeded via BitTorrent.
#
# Copyright © 2010 Matija "hook" Šuklje <matija@suklje.name>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Source code is available on: <http://gitorious.org/dashing-freemooina-cow/moo-cow>
#
# Usage:
# 1) change MUSIC_DIR accordingly;
# 2) in the same directory as 'moo-cow.sh' create file 'album_list';
# 3) edit 'album_list' and put a Jamendo album ID you want to download in each line;
# 4) run 'moo-cow.sh'

# Which directories to use.
# User: Please change accordingly.
MUSIC_DIR="~/Glasba"			# Where you store, play and seed from you music.
MUSIC_TMP_DIR="/tmp/moo-cow-"$USER	# Temporary directory for moo-cow to operate in; CAUTION: this *whole* dir is deleted by the script.

# Make the temporary directory.
mkdir -p $MUSIC_TMP_DIR

for ALBUMID in `cat album_list`
do
	# Download the album.
	wget 'http://www.jamendo.com/get/album/id/album/archiverestricted/redirect/'$ALBUMID'/?p2pnet=bittorrent\&are=ogg3' -P $MUSIC_TMP_DIR

	# Unzip the album into the music directory.
	for FILE in $MUSIC_TMP_DIR/*.zip
	do
		FILE_NAME=${FILE##*/}					# Crop file name.
		ALBUM_NAME=${FILE_NAME%.zip}				# Delete ".zip" extention.
		unzip -o "$FILE" -d "$MUSIC_DIR"/"$ALBUM_NAME"		# Unzip into the appropriate album directory.
	done
	
	# Cleanup / delete the temporary directory.
	rm -R $MUSIC_TMP_DIR
done
